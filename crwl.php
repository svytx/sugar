<?php
namespace Sugar;

class Crwl {
    function setUrl($url) {
        $this->url = $url;
    }

    function setCurlOpts($o) {
        $this->curlOpts = $o;
    }
    
    function curl_err($errno, $msg) {
        die('curl error: ['.$errno.']: '.$msg);
    }
    
    function get($getInfo = false) {
        $o = array(
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_VERBOSE => 1,
            CURLOPT_HEADER => 0
        );

        if (isset($this->curlOpts)) $o = $this->curlOpts + $o;

        $ch = curl_init();
        curl_setopt_array($ch, $o);
        $data = curl_exec($ch);
        if ($getInfo) $info = curl_getinfo($ch);
        
        if ($data === false) {
            $this->curl_err(curl_errno($ch), curl_error($ch));
        }

        curl_close($ch);

        return ($getInfo ? array($data, $info): $data);
    }
}
?>
